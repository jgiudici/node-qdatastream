var qds = require('..'),
    expect = require('chai').expect;

describe('QDataStream(read)', function() {
  describe('numbers', function() {
    it('should decode a qint8', function(done) {
      var buffer = new Buffer('1g==', 'base64');
      var decoded = qds.readQInt8(buffer);
      expect(decoded).to.equal(-42);
      done();
    });

    it('should decode a quint8', function(done) {
      var buffer = new Buffer('Kg==', 'base64');
      var decoded = qds.readQUInt8(buffer);
      expect(decoded).to.equal(42);
      done();
    });

    it('should decode a qint16', function(done) {
      var buffer = new Buffer('/9Y=', 'base64');
      var decoded = qds.readQInt16(buffer);
      expect(decoded).to.equal(-42);
      done();
    });

    it('should decode a quint16', function(done) {
      var buffer = new Buffer('ACo=', 'base64');
      var decoded = qds.readQUInt16(buffer);
      expect(decoded).to.equal(42);
      done();
    });

    it('should decode a qint32', function(done) {
      var buffer = new Buffer('////1g==', 'base64');
      var decoded = qds.readQInt32(buffer);
      expect(decoded).to.equal(-42);
      done();
    });

    it('should decode a quint32', function(done) {
      var buffer = new Buffer('AAAAKg==', 'base64');
      var decoded = qds.readQUInt32(buffer);
      expect(decoded).to.equal(42);
      done();
    });

    it('should decode a double', function(done) {
      var buffer = new Buffer('QCiuFHrhR64=', 'base64');
      var decoded = qds.readDouble(buffer);
      expect(decoded).to.equal(12.34);
      done();
    });
  });

  describe('booleans', function() {
    it('should decode a bool', function(done) {
      var buffer = new Buffer('AQ==', 'base64');
      var decoded = qds.readBool(buffer);
      expect(decoded).to.equal(true);
      done();
    });
  });

  describe('strings', function() {
    it('should decode an empty QString', function(done) {
      var buffer = new Buffer('/////w==', 'base64');
      var decoded = qds.readString(buffer);
      expect(decoded).to.equal('');
      done();
    });

    it('should decode a QString', function(done) {
      var buffer = new Buffer('AAAAFgB0AGUAcwB0ACAAcwB0AHIAaQBuAGc=', 'base64');
      var decoded = qds.readString(buffer);
      expect(decoded).to.equal('test string');
      done();
    });
  });

  describe('variants', function() {
    it('should decode QString QVariant', function(done) {
      var buffer = new Buffer('AAAACgAAAAAWAHQAZQBzAHQAIABzAHQAcgBpAG4AZw==', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.equal('test string');
      done();
    });

    it('should decode a null QVariant', function(done) {
      var buffer = new Buffer('AAAAAAH/////', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.be.null;
      done();
    });

    it('should decode bool QVariant', function(done) {
      var buffer = new Buffer('AAAAAQAB', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.equal(true);
      done();
    });

    it('should decode a qint8 QVariant', function(done) {
      var buffer = new Buffer('AAAAAgD////W', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.equal(-42);
      done();
    });

    it('should decode a quint8 QVariant', function(done) {
      var buffer = new Buffer('AAAAAgAAAAAq', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.equal(42);
      done();
    });

    it('should decode a qint16 QVariant', function(done) {
      var buffer = new Buffer('AAAAAgD////W', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.equal(-42);
      done();
    });

    it('should decode a quint16 QVariant', function(done) {
      var buffer = new Buffer('AAAAAgAAAAAq', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.equal(42);
      done();
    });

    it('should decode a qint32 QVariant', function(done) {
      var buffer = new Buffer('AAAAAgD////W', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.equal(-42);
      done();
    });

    it('should decode a quint32 QVariant', function(done) {
      var buffer = new Buffer('AAAAAwAAAAAq', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.equal(42);
      done();
    });

    it('should decode a double QVariant', function(done) {
      var buffer = new Buffer('AAAABgBAKK4UeuFHrg==', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.equal(12.34);
      done();
    });

    it('should decode a QStringList QVariant', function(done) {
      var buffer = new Buffer('AAAACwAAAAACAAAAEABQAG8AcwBpAHQAaQBvAG4AAAAGADAAeAAw', 'base64');
      var decoded = qds.readVariant(buffer);
      expect(decoded).to.eql(['Position', '0x0']);
      done();
    });
  });

  describe('variant lists', function() {
    it('should decode a QVariantList of a single QString', function(done) {
      var buffer = new Buffer('AAAACQAAAAABAAAACgAAAAAWAHQAZQBzAHQAIABzAHQAcgBpAG4AZw==', 'base64');
      var decoded = qds.readVariantList(buffer);
      expect(decoded).to.eql(['test string']);
      done();
    });

    it('should decode a QVariantList of multiple QStrings', function(done) {
      var buffer = new Buffer('AAAACQAAAAADAAAACgAAAAAOAHMAdAByAGkAbgBnADEAAAAKAAAAAA4AcwB0AHIAaQBuAGcAMgAAAAoAAAAADgBzAHQAcgBpAG4AZwAz', 'base64');
      var decoded = qds.readVariantList(buffer);
      expect(decoded).to.eql(['string1', 'string2', 'string3']);
      done();
    });
  });

  describe('variant maps', function() {
    it('should decode a QVariantMap of QString', function(done) {
      var buffer = new Buffer('AAAACAAAAAABAAAAFAB0AGUAcwB0AFMAdAByAGkAbgBnAAAACgAAAAAWAHQAZQBzAHQAIABzAHQAcgBpAG4AZw==', 'base64');
      var decoded = qds.readVariantMap(buffer);
      expect(decoded).to.eql({testString: 'test string'});
      done();
    });
  });

});
